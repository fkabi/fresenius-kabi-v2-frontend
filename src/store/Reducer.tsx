import { createBrowserHistory } from "history";
import { applyMiddleware, combineReducers, createStore } from "redux";
import createSagaMiddleware from "redux-saga";
import { logout } from "../api/fetch/fetch";
import { AuthContext } from "../context/AuthContext";
import { ToolBoxData } from "../interfaces/ToolBoxData";
import { AuthActionTypes, LOGIN, LOGOUT } from "./AuthActions";
import mySaga from "./Sagas";
import {
  LoadingAction,
  LOADING_FINISHED,
  LOADING_STARTED,
  ToolBoxAction,
} from "./ToolBoxActions";

const initialAuthState: AuthContext = {
  authenticated: false,
  logoutToken: undefined,
  token: undefined,
  user: undefined,
  tokenType: undefined,
  accessToken: undefined,
  expiresIn: undefined,
  refreshToken: undefined,
  msg: undefined,
};

const initialDataState: ToolBoxData = {
  goals: [],
  symptoms: [],
  medications: [],
  notes: [],
  error: "",
  msg: "",
};

export function authReducer(
  state = initialAuthState,
  { type, userData, msg }: AuthActionTypes
): AuthContext {
  switch (type) {
    case LOGIN: {
      sessionStorage.setItem("token", JSON.stringify(userData));

      return {
        authenticated: true,
        user: userData?.current_user,
        token: userData?.csrf_token,
        logoutToken: userData?.logout_token,
        tokenType: userData?.token_type,
        expiresIn: userData?.expires_in,
        accessToken: userData?.access_token,
        refreshToken: userData?.refresh_token,
      };
    }

    case LOGOUT: {
      logout(
        () => {},
        () => {}
      );
      sessionStorage.clear();

      createBrowserHistory().push("/");
      window.location.reload();

      return { msg: msg, ...initialAuthState };
    }

    default:
      return state;
  }
}

export function toolBoxReducer(
  state = initialDataState,
  { type, data }: ToolBoxAction
): ToolBoxData {
  return {
    ...state,
    ...data,
  };
}

export function loadingReducer(
  state = false,
  { type }: LoadingAction
): boolean {
  switch (type) {
    case LOADING_STARTED: {
      return true;
    }
    case LOADING_FINISHED: {
      return false;
    }

    default:
      return state;
  }
}

const rootReducer = combineReducers({
  auth: authReducer,
  loading: loadingReducer,
  toolbox: toolBoxReducer,
});

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(mySaga);

export type RootState = ReturnType<typeof rootReducer>;

export const authState = (state: RootState) => state.auth;
export const toolBoxState = (state: RootState) => state.toolbox;
export const loadingState = (state: RootState) => state.loading;
