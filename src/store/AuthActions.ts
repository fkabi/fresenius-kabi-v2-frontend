import { UserLoginData } from "../context/AuthContext";

export const LOGIN = "login";
export const LOGOUT = "logout";

interface LoginAction {
  type: typeof LOGIN;
  userData: UserLoginData;
  msg?: string;
}

interface LogoutAction {
  type: typeof LOGOUT;
  userData?: UserLoginData;
  msg?: string;
}

export type AuthActionTypes = LoginAction | LogoutAction;

export function login(userData: UserLoginData): LoginAction {
  return {
    type: LOGIN,
    userData: userData,
  };
}

export function logout(msg: string): LogoutAction {
  sessionStorage.clear();

  return {
    type: LOGOUT,
    msg: msg,
  };
}
