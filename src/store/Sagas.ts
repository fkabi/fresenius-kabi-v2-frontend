import _ from "lodash";
import { call, put, takeLatest } from "redux-saga/effects";
import { Api } from "../api/fetch/Api";
import {
  LOADING_FINISHED,
  TOOLBOX_FETCH_FAILED,
  TOOLBOX_FETCH_REQUESTED,
  TOOLBOX_FETCH_SUCCEEDED,
} from "./ToolBoxActions";

function mapResponseToClientFormat(data) {
  return _.flattenDeep(Object.values(data));
}

function* fetchToolBoxData() {
  try {
    const medications = mapResponseToClientFormat(
      (yield call(Api.getMedications)).data.content
    );
    const symptoms = mapResponseToClientFormat(
      (yield call(Api.getSymptoms)).data.content
    );
    const goals = mapResponseToClientFormat(
      (yield call(Api.getGoals)).data.content
    );
    const notes = mapResponseToClientFormat(
      (yield call(Api.getNotes)).data.content
    );

    yield put({
      type: TOOLBOX_FETCH_SUCCEEDED,
      data: {
        medications: medications,
        symptoms: symptoms,
        goals: goals,
        notes,
      },
    });
    yield put({ type: LOADING_FINISHED });
  } catch (e) {
    console.log(e);
    yield put({ type: TOOLBOX_FETCH_FAILED, message: e.message });
    yield put({ type: LOADING_FINISHED });
  }
}

function* mySaga() {
  yield takeLatest(TOOLBOX_FETCH_REQUESTED, fetchToolBoxData);
}

export default mySaga;
