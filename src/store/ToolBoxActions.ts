import { ToolBoxData } from "../interfaces/ToolBoxData";

export const TOOLBOX_FETCH_SUCCEEDED = "TOOLBOX_FETCH_SUCCEEDED";
export const TOOLBOX_FETCH_FAILED = "TOOLBOX_FETCH_FAILED";
export const TOOLBOX_FETCH_REQUESTED = "TOOLBOX_FETCH_REQUESTED";
export const TOOLBOX_DATA_UPDATE = "TOOLBOX_DATA_UPDATE";
export const LOADING_FINISHED = "LOADING_FINISHED";
export const LOADING_STARTED = "LOADING_STARTED";

export interface ToolBoxAction {
  type:
    | typeof TOOLBOX_FETCH_SUCCEEDED
    | typeof TOOLBOX_FETCH_FAILED
    | typeof TOOLBOX_FETCH_REQUESTED
    | typeof TOOLBOX_DATA_UPDATE;
  data?: ToolBoxData;
}

export interface LoadingAction {
  type: typeof LOADING_FINISHED | typeof LOADING_STARTED;
}

export function saveToolBox(data: ToolBoxData): ToolBoxAction {
  return {
    type: TOOLBOX_FETCH_SUCCEEDED,
    data: data,
  };
}

export function failedRequest(data: ToolBoxData): ToolBoxAction {
  return {
    type: TOOLBOX_FETCH_SUCCEEDED,
    data: data,
  };
}

export function updateData(data: ToolBoxData): ToolBoxAction {
  return {
    type: TOOLBOX_DATA_UPDATE,
    data: data,
  };
}

export function loading(): LoadingAction {
  return {
    type: LOADING_STARTED,
  };
}

export function finishedLoading(): LoadingAction {
  return {
    type: LOADING_FINISHED,
  };
}
