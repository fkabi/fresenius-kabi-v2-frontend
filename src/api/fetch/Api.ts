import axios, { AxiosRequestConfig } from "axios";
import _ from "lodash";
import { getUserData } from "../../context/AuthContext";
import { LOGOUT } from "../../store/AuthActions";
import { store } from "../../store/Reducer";

const baseUrl = process.env.REACT_APP_BASE_URL || "";

axios.defaults.headers.post["Content-Type"] = "application/json";

function sendRequest(config: AxiosRequestConfig) {
  const token = getUserData().access_token;

  config.headers = {
    "Content-Type": "application/json",
  };

  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }

  if (config.method === "post" && !config.data) {
    config.data = {};
  }

  return axios(config)
    .then(function (response) {
      if (
        response.data.success ||
        response.data.data.content?.content === "[]"
      ) {
        return response.data;
      } else if (response.data && response.data.data.content) {
        throw new Error(response.data.data.content);
      } else if (response.data || !response.data.success) {
        throw new Error(response.data.message);
      } else {
        throw new Error(response.statusText);
      }
    })
    .catch((error) => {
      if (error.response?.status === 401) {
        store.dispatch({ type: LOGOUT, msg: error });
      } else {
        throw new Error(error.message);
      }
    });
}

export class API {
  registerHCP(data: any) {
    var config = {
      method: "post" as "post",
      url: `${baseUrl}/api/json/hcp/register?_format=json`,
      data: data,
    };

    return sendRequest(config);
  }

  registerPatient(data: Object) {
    var config = {
      method: "post" as "post",
      url: `${baseUrl}/api/json/patient/register?_format=json`,
      data: data,
    };

    sendRequest(config);
  }

  placeCoolbagOrder(data) {
    const config = {
      method: "post" as "post",
      url: `${baseUrl}/place-order?_format=json`,
      data: data,
    };

    sendRequest(config);
  }

  getCoolbagOrder() {
    var config = {
      method: "get" as "get",
      url: `${baseUrl}/coolbag-order-rest-api/get-order?_format=json`,
    };

    return sendRequest(config);
  }

  getUserConditions() {
    let config = {
      method: "get" as "get",
      url: `${baseUrl}/api/json/get-conditions?_format=json`,
    };

    sendRequest(config);
  }

  logout() {
    let config = {
      method: "get" as "get",
      url: `${baseUrl}/user/logout?_format=json`,
    };

    sendRequest(config);
  }

  getUserDailySymptom(interval) {
    var config = {
      method: "get" as "get",
      url: `${baseUrl}/daily-user-symptom-rest-api/get-daily-user-symptoms?_format=json&interval=${interval}`,
    };

    sendRequest(config);
  }

  getMedications() {
    const config = {
      method: "get" as "get",
      url: `${baseUrl}/medication-rest-api/get-medications?_format=json`,
    };

    return sendRequest(config);
  }

  getGoals() {
    const config = {
      method: "get" as "get",
      url: `${baseUrl}/goal-rest-api/get-goals?_format=json`,
    };

    return sendRequest(config);
  }

  getSymptoms() {
    const config = {
      method: "get" as "get",
      url: `${baseUrl}/symptom-rest-api/get-symptoms?_format=json`,
    };

    return sendRequest(config);
  }

  createSymptom(data) {
    const config = {
      method: "post" as "post",
      url: `${baseUrl}/symptom-rest-api/create-symptom?_format=json`,
      data: data,
    };

    sendRequest(config);
  }

  modifySymptom(data) {
    const config = {
      method: "post" as "post",
      url: `${baseUrl}/symptom-rest-api/${data.id[0].value}/modify-symptom?_format=json`,
      data: data,
    };

    sendRequest(config);
  }

  deleteSymptom(data) {
    const config = {
      method: "post" as "post",
      url: `${baseUrl}/symptom-rest-api/${data}/delete-symptom?_format=json`,
    };

    sendRequest(config);
  }

  createMedication(data) {
    const config = {
      method: "post" as "post",
      url: `${baseUrl}/medication-rest-api/create-medication?_format=json`,
      data: data,
    };

    sendRequest(config);
  }

  modifyMedication(data) {
    const config = {
      method: "post" as "post",
      url: `${baseUrl}/medication-rest-api/${data.id[0].value}/modify-medication?_format=json`,
      data: _.omit(data, "id"),
    };

    sendRequest(config);
  }

  deleteMedication(data) {
    const config = {
      method: "post" as "post",
      url: `${baseUrl}/medication-rest-api/${data}/delete-medication?_format=json`,
    };

    sendRequest(config);
  }

  createGoal(data) {
    var config = {
      method: "post" as "post",
      url: `${baseUrl}/goal-rest-api/create-goal?_format=json`,
      data: data,
    };

    sendRequest(config);
  }

  modifyGoal(data) {
    var config = {
      method: "post" as "post",
      url: `${baseUrl}/goal-rest-api/${data.id[0].value}/modify-goal?_format=json`,
      data: _.omit(data, "id"),
    };

    return sendRequest(config);
  }

  deleteGoal(data) {
    var config = {
      method: "post" as "post",
      url: `${baseUrl}/goal-rest-api/${data}/delete-goal?_format=json`,
    };

    sendRequest(config);
  }

  createNote(data) {
    var config = {
      method: "post" as "post",
      url: `${baseUrl}/notes-rest-api/create-note?_format=json`,
      data: data,
    };

    sendRequest(config);
  }

  modifyNote(data) {
    var config = {
      method: "post" as "post",
      url: `${baseUrl}/notes-rest-api/${data.id[0].value}/modify-note?_format=json`,
      data: _.omit(data, "id"),
    };

    sendRequest(config);
  }

  deleteNote(data) {
    var config = {
      method: "post" as "post",
      url: `${baseUrl}/notes-rest-api/${data}/delete-note?_format=json`,
    };

    sendRequest(config);
  }

  getNotes() {
    const config = {
      method: "get" as "get",
      url: `${baseUrl}/notes-rest-api/get-notes?type=notes&_format=json`,
    };

    return sendRequest(config);
  }

  createDailyUserSymptom(data) {
    const config = {
      method: "post" as "post",
      url: `${baseUrl}/daily-user-symptom-rest-api/create-daily-user-symptoms?_format=json`,
      data: data,
    };

    return sendRequest(config);
  }

  createMedicationReport(data) {
    const config = {
      method: "post" as "post",
      url: `${baseUrl}/user-medication-report-rest-api/create-user-medication-report?_format=json`,
      data: data,
    };

    return sendRequest(config);
  }

  getUserMedicationReport() {
    const config = {
      method: "get" as "get",
      url: `${baseUrl}/user-medication-report-rest-api/get-user-medication-report?_format=json`,
    };

    return sendRequest(config);
  }
}

export const Api = new API();
