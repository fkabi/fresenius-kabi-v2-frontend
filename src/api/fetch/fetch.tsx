import axios, { AxiosRequestConfig } from "axios";
import _ from "lodash";
import { getUserData } from "../../context/AuthContext";
import { LOGOUT } from "../../store/AuthActions";
import { store } from "../../store/Reducer";

const baseUrl = process.env.REACT_APP_BASE_URL || "";
const client_secret = process.env.REACT_APP_CLIENT_SECRET || "";
const client_id = process.env.REACT_APP_CLIENT_ID;

axios.defaults.headers.post["Content-Type"] = "application/json";

export function login(userData: any, successFn, errorFn) {
  const FormData = require("form-data");
  let data = new FormData();
  data.append("grant_type", "password");
  data.append("client_id", client_id);
  data.append("client_secret", client_secret);
  data.append("username", userData.name);
  data.append("password", userData.pass);

  let config = {
    method: "post" as "post",
    url: `${baseUrl}/oauth/token`,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    data: data,
  };

  sendRequest(config, successFn, errorFn, true);
}

export function refreshToken(successFn, errorFn) {
  const token = getUserData().refresh_token;

  const FormData = require("form-data");
  let data = new FormData();
  data.append("grant_type", "refresh_token");
  data.append("client_id", client_id);
  data.append("client_secret", client_secret);
  data.append("refresh_token", token);

  let config = {
    method: "post" as "post",
    url: `${baseUrl}/oauth/token`,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    data: data,
  };

  sendRequest(config, successFn, errorFn, true);
}

export function registerHCP(data: any, successFn, errorFn) {
  var config = {
    method: "post" as "post",
    url: `${baseUrl}/api/json/hcp/register?_format=json`,
    headers: {
      "Content-Type": "application/json",
    },
    data: data,
  };

  sendRequest(config, successFn, errorFn);
}

export function registerPatient(data: Object, successFn, errorFn) {
  var config = {
    method: "post" as "post",
    url: `${baseUrl}/api/json/patient/register?_format=json`,
    headers: {
      "Content-Type": "application/json",
    },
    data: data,
  };

  sendRequest(config, successFn, errorFn);
}

export function placeCoolbagOrder(data, successFn, errorFn) {
  const config: AxiosRequestConfig = {
    method: "post" as "post",
    url: `${baseUrl}/place-order?_format=json`,
    data: data,
  };

  sendRequest(config, successFn, errorFn);
}

export function getCoolbagOrder(successFn, errorFn) {
  var config = {
    method: "get" as "get",
    url: `${baseUrl}/coolbag-order-rest-api/get-order?_format=json`,
    headers: {
      "Content-Type": "application/json",
    },
  };

  sendRequest(config, successFn, errorFn);
}

export function getUserConditions(successFn, errorFn) {
  let config = {
    method: "get" as "get",
    url: `${baseUrl}/api/json/get-conditions?_format=json`,
    headers: {},
  };

  sendRequest(config, successFn, errorFn);
}

export function logout(successFn, errorFn) {
  let config = {
    method: "get" as "get",
    url: `${baseUrl}/user/logout?_format=json`,
    headers: {},
  };

  sendRequest(config, successFn, errorFn);
}

export function getUserDailySymptom(interval, successFn, errorFn) {
  var config = {
    method: "get" as "get",
    url: `${baseUrl}/daily-user-symptom-rest-api/get-daily-user-symptoms?_format=json&interval=${interval}`,
    headers: {},
  };

  sendRequest(config, successFn, errorFn);
}

function sendRequest(
  config: AxiosRequestConfig,
  successFn,
  errorFn,
  checkValidStatus = false
) {
  const token = getUserData().access_token;

  config.headers = {
    "Content-Type": "application/json",
  };

  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }

  if (config.method === "post" && !config.data) {
    config.data = {};
  }

  axios(config)
    .then(function (response) {
      if (
        response.data.success ||
        (checkValidStatus && response.status === 200)
      ) {
        successFn(response.data);
      } else {
        errorFn(handleError({ response }));
      }
    })
    .catch(function (error) {
      errorFn(handleError(error));
    });
}

function handleError(error) {
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    switch (error.response.status) {
      case 500:
        return "Service unavailable";
      case 415:
        return "Client error";
      default: {
        const msg = error.response.data?.message;
        if (
          msg.includes("Login timed out") ||
          msg.includes("Access token is invalid") ||
          msg.includes("No authentication credentials provided.")
        ) {
          store.dispatch({ type: LOGOUT, msg: error });
        }
        return error.response.data?.message;
      }
    }
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    return "No response from the server";
  } else {
    // Something happened in setting up the request that triggered an Error
    return "Service unavailable";
  }
}

export function getMedications(successFn, errorFn) {
  const config = {
    method: "get" as "get",
    url: `${baseUrl}/medication-rest-api/get-medications?_format=json`,
  };

  sendRequest(config, successFn, errorFn);
}

export function getGoals(successFn, errorFn) {
  const config = {
    method: "get" as "get",
    url: `${baseUrl}/goal-rest-api/get-goals?_format=json`,
  };

  sendRequest(config, successFn, errorFn);
}

export function getSymptoms(successFn, errorFn) {
  const config = {
    method: "get" as "get",
    url: `${baseUrl}/symptom-rest-api/get-symptoms?_format=json`,
  };

  sendRequest(config, successFn, errorFn);
}

export function createSymptom(data, successFn, errorFn) {
  const config = {
    method: "post" as "post",
    url: `${baseUrl}/symptom-rest-api/create-symptom?_format=json`,
    data: data,
  };

  sendRequest(config, successFn, errorFn);
}

export function modifySymptom(data, successFn, errorFn) {
  const config = {
    method: "post" as "post",
    url: `${baseUrl}/symptom-rest-api/${data.id[0].value}/modify-symptom?_format=json`,
    data: data,
  };

  sendRequest(config, successFn, errorFn);
}

export function deleteSymptom(data, successFn, errorFn) {
  const config = {
    method: "post" as "post",
    url: `${baseUrl}/symptom-rest-api/${data}/delete-symptom?_format=json`,
  };

  sendRequest(config, successFn, errorFn);
}

export function createMedication(data, successFn, errorFn) {
  const config = {
    method: "post" as "post",
    url: `${baseUrl}/medication-rest-api/create-medication?_format=json`,
    data: data,
  };

  sendRequest(config, successFn, errorFn);
}

export function modifyMedication(data, successFn, errorFn) {
  const config = {
    method: "post" as "post",
    url: `${baseUrl}/medication-rest-api/${data.id[0].value}/modify-medication?_format=json`,
    data: _.omit(data, "id"),
  };

  sendRequest(config, successFn, errorFn);
}

export function deleteMedication(data, successFn, errorFn) {
  const config = {
    method: "post" as "post",
    url: `${baseUrl}/medication-rest-api/${data}/delete-medication?_format=json`,
  };

  sendRequest(config, successFn, errorFn);
}

export function createGoal(data, successFn, errorFn) {
  var config = {
    method: "post" as "post",
    url: `${baseUrl}/goal-rest-api/create-goal?_format=json`,
    data: data,
  };

  sendRequest(config, successFn, errorFn);
}

export function modifyGoal(data, successFn, errorFn) {
  var config = {
    method: "post" as "post",
    url: `${baseUrl}/goal-rest-api/${data.id[0].value}/modify-goal?_format=json`,
    data: _.omit(data, "id"),
  };

  sendRequest(config, successFn, errorFn);
}

export function deleteGoal(data, successFn, errorFn) {
  var config = {
    method: "post" as "post",
    url: `${baseUrl}/goal-rest-api/${data}/delete-goal?_format=json`,
  };

  sendRequest(config, successFn, errorFn);
}

export function createNote(data, successFn, errorFn) {
  var config = {
    method: "post" as "post",
    url: `${baseUrl}/notes-rest-api/create-note?_format=json`,
    data: data,
  };

  sendRequest(config, successFn, errorFn);
}

export function modifyNote(data, successFn, errorFn) {
  var config = {
    method: "post" as "post",
    url: `${baseUrl}/notes-rest-api/${data.id[0].value}/modify-note?_format=json`,
    data: _.omit(data, "id"),
  };

  sendRequest(config, successFn, errorFn);
}

export function deleteNote(data, successFn, errorFn) {
  var config = {
    method: "post" as "post",
    url: `${baseUrl}/notes-rest-api/${data}/delete-note?_format=json`,
  };

  sendRequest(config, successFn, errorFn);
}

export function getNotes(successFn, errorFn) {
  const config = {
    method: "get" as "get",
    url: `${baseUrl}/notes-rest-api/get-notes?_format=json`,
  };

  sendRequest(config, successFn, errorFn);
}

export function createDailyUserSymptom(data, successFn, errorFn) {
  const config = {
    method: "post" as "post",
    url: `${baseUrl}/daily-user-symptom-rest-api/create-daily-user-symptoms?_format=json`,
    data: data,
  };

  sendRequest(config, successFn, errorFn);
}

export function createMedicationReport(data, successFn, errorFn) {
  const config = {
    method: "post" as "post",
    url: `${baseUrl}/user-medication-report-rest-api/create-user-medication-report?_format=json`,
    data: data,
  };

  sendRequest(config, successFn, errorFn);
}
