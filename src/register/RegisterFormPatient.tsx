import { yupResolver } from "@hookform/resolvers/yup";
import {
  Checkbox,
  FormControlLabel,
  FormHelperText,
  MenuItem,
} from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import React, { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import * as yup from "yup";
import { getUserConditions } from "../api/fetch/fetch";
import {
  confirm_password,
  email,
  condition,
  access_code,
  optIn,
  password,
} from "../constants/constants";

const schema = yup.object().shape({
  condition: condition,
  access_code: access_code,
  email: email,
  password: password,
  confirm_password: confirm_password,
  optIn: optIn,
});

interface Condition {
  id: number;
  label: string;
  condition: string;
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  control: {
    width: "100%",
  },
}));

export default function RegisterFormPatient(props) {
  const classes = useStyles();
  const { register, handleSubmit, errors, control } = useForm<any>({
    resolver: yupResolver(schema),
  });
  const [condition, setCondition] = useState("");
  const [conditions, setConditions] = useState(new Array<Condition>());

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setCondition(event.target.value as string);
  };

  React.useEffect(() => {
    getUserConditions(
      (response) =>
        setConditions(Object.values(response.data.conditions) as Condition[]),
      (error) => console.log("error", error)
    );
  }, []);

  return (
    <Container component="div" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form
          className={classes.form}
          noValidate
          onSubmit={handleSubmit(props.onPatientRegister)}
        >
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                helperText={errors?.email?.message}
                error={errors.email ? true : false}
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12} sm={12}>
              <Controller
                as={
                  <TextField
                    id="condition"
                    select
                    label="Condition"
                    value={condition}
                    onChange={handleChange}
                    helperText={errors?.condition?.message}
                    error={errors?.condition ? true : false}
                    variant="outlined"
                  >
                    {conditions.map((option) => (
                      <MenuItem key={option.id} value={option.id}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                }
                control={control}
                className={classes.control}
                name="condition"
                defaultValue={""}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="access"
                label="Access Code"
                name="access_code"
                autoComplete=""
                helperText={errors?.access_code?.message}
                error={errors.access_code ? true : false}
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                helperText={errors?.password?.message}
                error={errors?.password ? true : false}
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="confirm_password"
                label="Confirm Password"
                type="password"
                id="passwordConfirm"
                autoComplete="current-password"
                helperText={errors?.confirm_password?.message}
                error={errors?.confirm_password ? true : false}
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12}>
              <FormControlLabel
                control={
                  <Checkbox name="optIn" color="primary" inputRef={register} />
                }
                label="Send me emails"
              />
            </Grid>
            <Grid item xs={12}>
              <FormHelperText id="component-helper-text" error>
                {props.error}{" "}
              </FormHelperText>
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link to="/login" variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}
