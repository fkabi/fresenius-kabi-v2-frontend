import { yupResolver } from "@hookform/resolvers/yup";
import { FormHelperText, MenuItem } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import React, { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import * as yup from "yup";
import {
  confirm_password,
  country,
  email,
  firstName,
  healthcare_organization,
  lastName,
  password,
  registration_number,
  title,
} from "../constants/constants";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="">
        Kabicare v2
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const schema = yup.object().shape({
  title: title,
  firstName: firstName,
  lastName: lastName,
  country: country,
  healthcare_organization: healthcare_organization,
  email: email,
  password: password,
  confirm_password: confirm_password,
  registration_number: registration_number,
});

export interface RegisterFormHCP {
  title: string;
  firstName: string;
  lastName: string;
  country: string;
  healthcare_organization: string;
  email: string;
  password: string;
  registration_number: string;
  confirm_password: string;
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  control: {
    width: "100%",
  },
}));

const titles = ["Dr", "Prof", "Mr", "Mrs", "Miss", "Ms"];
const countries = ["England", "Wales", "Scotland", "Northern Ireland"];

export default function HCPRegisterForm(props) {
  const classes = useStyles();
  const { register, handleSubmit, errors, control } = useForm<RegisterFormHCP>({
    resolver: yupResolver(schema),
  });
  const [title, setTitle] = useState("");
  const [country, setCountry] = useState("");

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setTitle(event.target.value as string);
  };

  const handleCountryChange = (
    event: React.ChangeEvent<{ value: unknown }>
  ) => {
    setCountry(event.target.value as string);
  };

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form
          className={classes.form}
          noValidate
          onSubmit={handleSubmit(props.onHCPRegister)}
        >
          <Grid container spacing={2}>
            <Grid item xs={3} sm={3}>
              <Controller
                as={
                  <TextField
                    id="title"
                    select
                    label="Title"
                    value={title}
                    onChange={handleChange}
                    helperText={errors?.title?.message}
                    error={errors?.title ? true : false}
                    variant="outlined"
                  >
                    {titles.map((option) => (
                      <MenuItem key={option} value={option}>
                        {option}
                      </MenuItem>
                    ))}
                  </TextField>
                }
                control={control}
                name="title"
                defaultValue={""}
                className={classes.control}
              />
            </Grid>
            <Grid item xs={9} sm={9}>
              <TextField
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstname"
                label="First Name"
                autoFocus
                helperText={errors?.firstName?.message}
                error={errors?.firstName ? true : false}
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12} sm={12}>
              <TextField
                autoComplete="lname"
                name="lastName"
                variant="outlined"
                required
                fullWidth
                id="lastname"
                label="Last Name"
                autoFocus
                helperText={errors?.lastName?.message}
                error={errors?.lastName ? true : false}
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12} sm={12}>
              <Controller
                as={
                  <TextField
                    id="country"
                    select
                    label="Country"
                    value={country}
                    onChange={handleCountryChange}
                    helperText={errors?.country?.message}
                    error={errors?.country ? true : false}
                    variant="outlined"
                  >
                    {countries.map((option) => (
                      <MenuItem key={option} value={option}>
                        {option}
                      </MenuItem>
                    ))}
                  </TextField>
                }
                control={control}
                className={classes.control}
                name="country"
                defaultValue={""}
              />
            </Grid>
            <Grid item xs={12} sm={12}>
              <TextField
                autoComplete="organization"
                name="healthcare_organization"
                variant="outlined"
                required
                fullWidth
                id="org"
                label="Healthcare Organization"
                autoFocus
                helperText={errors?.healthcare_organization?.message}
                error={errors?.healthcare_organization ? true : false}
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                helperText={errors?.email?.message}
                error={errors.email ? true : false}
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                helperText={errors?.password?.message}
                error={errors?.password ? true : false}
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="confirm_password"
                label="Confirm Password"
                type="password"
                id="passwordConfirm"
                autoComplete="current-password"
                helperText={errors?.confirm_password?.message}
                error={errors?.confirm_password ? true : false}
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="regnumber"
                label="Registration Number"
                name="registration_number"
                autoComplete=""
                helperText={errors?.registration_number?.message}
                error={errors.registration_number ? true : false}
                inputRef={register}
              />
            </Grid>
            <FormHelperText id="component-helper-text" error>
              {props.error}{" "}
            </FormHelperText>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link to="/login" variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
}
