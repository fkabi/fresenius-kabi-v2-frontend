import _ from "lodash";
import { cloneElement, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { registerHCP, registerPatient } from "../api/fetch/fetch";
import WithLoader from "../component/WithLoader";
import { authState } from "../store/Reducer";
import mapDataToEntityFormat from "../util/mapDataToEntityFormat";

export function RegisterController({ children }) {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const { authenticated } = useSelector(authState);
  const [error, setError] = useState("");

  const onHCPRegister = (data) => {
    setLoading(true);

    data.name = data.firstName + " " + data.lastName;

    data = _.omit(data, ["firstName", "lastName"]);

    const formData = mapData(data);
    registerHCP(
      formData,
      (result) => {
        history.push("/login");
        setLoading(false);
      },
      (error) => {
        setError(error);
        setLoading(false);
      }
    );
  };

  const onPatientRegister = (data) => {
    setLoading(true);

    data.name = data.email;

    const formData = mapDataToEntityFormat(data);

    data.conditions = data.condition;

    delete data.condition;

    registerPatient(
      formData,
      (result) => {
        setLoading(false);
        history.push("/login");
      },
      (error) => {
        setError(error);
        setLoading(false);
      }
    );
  };

  if (authenticated) {
    history.push("/");
  }

  const Child = cloneElement(children, {
    onHCPRegister: onHCPRegister,
    onPatientRegister: onPatientRegister,
    error: error,
  });

  return (
    <>
      <WithLoader loading={loading}>{Child}</WithLoader>
    </>
  );
}

export function mapData(data: any): any {
  return _.mapValues(data, (value, key) => {
    if (key.includes("ref")) {
      return [{ target_id: value }];
    }

    return [{ value: value }];
  });
}

export function mapDataToObject(data: any): any {
  return _.mapValues(data, (value, key) => {
    if (key.includes("condition")) {
      return [{ target_id: value }];
    }

    return { value: value };
  });
}
