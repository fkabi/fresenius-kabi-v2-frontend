import React, { cloneElement, useState } from "react";
import { useHistory } from "react-router-dom";
import { Api } from "../api/fetch/Api";
import { placeCoolbagOrder } from "../api/fetch/fetch";
import WithLoader from "../component/WithLoader";

export interface CoolbagOrder {
  field_order_consent_checkbox: [{ value: number }];
  field_order_patient_name: [{ value: string }];
  field_order_patient_title: [{ value: string }];
  field_order_telephone_number: [{ value: string }];
  field_order_address_address: [{ value: string }];
  field_order_address_city: [{ value: string }];
  field_order_address_country: [{ value: string }];
  field_order_address_postal_code: [{ value: string }];
}

export function CoolbagController({ children }) {
  const history = useHistory();
  const [error, setError] = useState({});
  const [loading, setLoading] = useState(true);
  const [canOrder, setCanOrder] = useState(false);
  const [content, setContent] = useState("");

  const onSubmit = (data: any) => {
    setLoading(true);
    const addressLine = data.address.split(",").map((s) => s.trim());
    const address = addressLine[0]
      ? addressLine[0]
      : setError({ address: "Not a complete address" });
    const city = addressLine[1]
      ? addressLine[1]
      : setError({ address: "Not a complete address" });
    const country = addressLine[2]
      ? addressLine[2]
      : setError({ address: "Not a complete address" });

    const formData: CoolbagOrder = {
      field_order_address_address: [{ value: address }],
      field_order_address_country: [{ value: country }],
      field_order_address_city: [{ value: city }],
      field_order_address_postal_code: [{ value: data.postalcode }],
      field_order_consent_checkbox: [{ value: 1 }],
      field_order_patient_name: [{ value: data.name }],
      field_order_patient_title: [{ value: data.title }],
      field_order_telephone_number: [{ value: data.phonenumber }],
    };

    placeCoolbagOrder(
      formData,
      (response) => {
        history.push("/thankyou");
        setLoading(false);
      },
      (error) => {
        setError({ msg: error });
        setLoading(false);
      }
    );
  };

  React.useEffect(() => {
    Api.getCoolbagOrder()
      .then((response) => {
        setCanOrder(response.data.can_order && response.data.has_order);
        setContent(response.data.content);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
        setError(error.message);
        console.log("error", error);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const Child = cloneElement(children, {
    onSubmit: onSubmit,
    error: error,
    canOrder: canOrder,
    content: content,
  });

  return (
    <>
      <WithLoader loading={loading}>{Child}</WithLoader>
    </>
  );
}
