import { yupResolver } from "@hookform/resolvers/yup";
import { FormHelperText, MenuItem } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import LocalMallIcon from "@material-ui/icons/LocalMall";
import React, { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import * as yup from "yup";
import { address, name, title } from "../constants/constants";
import GoogleAutocomplete from "./AutoComplete";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  title: {
    textTransform: "uppercase",
  },
  description: {
    textAlign: "center",
    paddingTop: theme.spacing(5),
  },
  formControl: {
    width: "100%",
    minWidth: 80,
  },
}));

const titles = ["Mr", "Mrs", "Miss", "Dr"];

const schema = yup.object().shape({
  name: name,
  title: title,
  address: address,
  phonenumber: yup.string().required(),
});

export default function CoolbagForm(props) {
  const classes = useStyles();
  const { register, setValue, handleSubmit, errors, control } = useForm<any>({
    resolver: yupResolver(schema),
  });
  const [title, setTitle] = useState("");
  const [postCode, setPostCode] = useState("");

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setTitle(event.target.value as string);
  };

  const handlePostCodeChange = (postCode) => {
    setPostCode(postCode);
    setValue("postalcode", postCode, {
      shouldDirty: false,
      shouldValidate: true,
    });
  };

  return (
    <Container component="main" maxWidth="sm">
      {props.canOrder ? (
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LocalMallIcon />
          </Avatar>
          <Typography component="h1" variant="h5" className={classes.title}>
            Request a cool bag
          </Typography>
          <Typography
            component="h5"
            variant="h5"
            className={classes.description}
          >
            When travelling it’s vital to keep your IDACIO medication safe and
            at the right temperature. Complete the form below to receive one of
            our branded KabiCare cool bags for free.
          </Typography>
          <form
            className={classes.form}
            onSubmit={handleSubmit(props.onSubmit)}
          >
            <Grid container spacing={3}>
              <Grid item xs={3} sm={3}>
                <Controller
                  as={
                    <TextField
                      id="outlined-select-currency"
                      select
                      label="Title"
                      value={title}
                      onChange={handleChange}
                      helperText={errors?.title?.message}
                      error={errors?.title ? true : false}
                      variant="outlined"
                      className={classes.formControl}
                    >
                      {titles.map((option) => (
                        <MenuItem key={option} value={option}>
                          {option}
                        </MenuItem>
                      ))}
                    </TextField>
                  }
                  control={control}
                  name="title"
                  defaultValue={""}
                />
              </Grid>
              <Grid item xs={9} sm={9}>
                <TextField
                  autoComplete="fname"
                  name="name"
                  variant="outlined"
                  required
                  fullWidth
                  id="name"
                  label="Name"
                  error={errors?.name ? true : false}
                  helperText={errors?.name?.message}
                  inputRef={register}
                />
              </Grid>
              <Grid item xs={12}>
                <GoogleAutocomplete
                  autoComplete="address-line1"
                  name="address"
                  variant="outlined"
                  required
                  fullWidth
                  id="address-line1"
                  label="Address"
                  error={errors?.address ? true : false}
                  helperText={errors?.address?.message}
                  inputRef={register}
                  setPostCode={handlePostCodeChange}
                />
              </Grid>
              <Grid item xs={12}>
                <Controller
                  as={
                    <TextField
                      variant="outlined"
                      value={postCode}
                      onChange={(event) =>
                        handlePostCodeChange(event.target.value)
                      }
                      required
                      fullWidth
                      multiline
                      name="postalcode"
                      label="Postal Code"
                      id="postalcode"
                      autoComplete="postal-code"
                    />
                  }
                  control={control}
                  name="postalcode"
                  defaultValue={""}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  name="phonenumber"
                  label="Telephone Number"
                  id="phonenumber"
                  autoComplete="tel"
                  error={errors?.phonenumber ? true : false}
                  helperText={errors?.phonenumber?.message}
                  inputRef={register}
                />
              </Grid>
              <Grid item xs={12}>
                <FormHelperText id="component-helper-text" error>
                  {props.error}
                </FormHelperText>
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Submit
            </Button>
          </form>
        </div>
      ) : (
        <div dangerouslySetInnerHTML={{ __html: props.error }} />
      )}
    </Container>
  );
}
