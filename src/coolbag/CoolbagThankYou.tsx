import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React from "react";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="">
        Kabicare v2
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  title: {
    textTransform: "uppercase",
  },
  description: {
    textAlign: "center",
    paddingTop: theme.spacing(5),
  },
}));

export default function CoolbagThankYou(props) {
  const classes = useStyles();

  return (
    <Container component="main" maxWidth="sm">
      <div className={classes.paper}>
        <Typography component="h1" variant="h5" className={classes.title}>
          Request a cool bag
        </Typography>
        <Typography component="h5" variant="h5" className={classes.description}>
          Thank you. We will fulfil your request within X days and contact you
          via email with an estimated delivery time.
        </Typography>
        <Typography component="h5" variant="h5" className={classes.description}>
          If you wish to contact us, to update your delivery address or cancel
          the request entirely, please contact us via xxxx@kabicare.xxx.
        </Typography>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
}
