import { cloneElement, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { login } from "../api/fetch/fetch";
import WithLoader from "../component/WithLoader";
import { LOGIN } from "../store/AuthActions";
import { authState } from "../store/Reducer";
import {
  LOADING_STARTED,
  TOOLBOX_FETCH_REQUESTED,
} from "../store/ToolBoxActions";

export function LoginController({ children }) {
  const history = useHistory();
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const { authenticated, msg } = useSelector(authState);

  const onLogin = (data: any) => {
    setLoading(true);

    login(
      data,
      (result) => {
        dispatch({ type: LOGIN, userData: result });
        dispatch({ type: TOOLBOX_FETCH_REQUESTED });
        dispatch({ type: LOADING_STARTED });
        history.push("/");
      },
      (error) => {
        setError(error);
        setLoading(false);
      }
    );
  };

  const onError = (errors) => {
    console.log("errors", errors);
  };

  if (authenticated) {
    history.push("/");
  }

  const Child = cloneElement(children, {
    onSubmit: onLogin,
    onError: onError,
    error: error,
    msg: msg,
  });

  return <WithLoader loading={loading}>{Child}</WithLoader>;
}
