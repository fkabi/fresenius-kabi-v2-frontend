import * as yup from "yup";

export const title = yup
  .string()
  .required("Title is required")
  .matches(/^(Dr|Prof|Mr|Mrs|Miss|Ms)$/, { message: "Not a valid title" });
export const firstName = yup
  .string()
  .required("Firstname is required")
  .min(4)
  .max(50);
export const lastName = yup
  .string()
  .required("Lastname is required")
  .min(4)
  .max(50);
export const country = yup
  .string()
  .required("Country is required")
  .matches(/^(England|Wales|Scotland|Northern Ireland")$/, {
    message: "Not a valid country",
  });
export const healthcare_organization = yup
  .string()
  .required("Healthcare organization is required");
export const email = yup
  .string()
  .required("Email is required")
  .matches(/^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/, {
    message: "Not a valid email address",
  });
export const password = yup
  .string()
  .required("Password is required")
  .matches(/^.*(?=\S{6,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])[^"'\s]*$/, {
    message:
      "Password too weak. Must be at least 6 characters, contain at least one capital letter, contain at least one number.",
  });
export const confirm_password = yup
  .string()
  .required("Password confirm is required")
  .oneOf([yup.ref("password"), null], "Passwords must match");
export const registration_number = yup
  .string()
  .required("Registration number is required");
export const address = yup.string().required("Address is required");
export const phonenumber = yup
  .string()
  .required()
  .matches(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s./0-9]*$/, {
    message: "Not a valid phonenumber",
  });
export const name = yup.string().required("Name is required");
export const condition = yup.string().required("Condition is required");
export const access_code = yup
  .string()
  .required("Access Code is required")
  .min(3, "Must be minimum 3 characters")
  .max(32, "Must be below 32 characters");
export const optIn = yup.boolean();
