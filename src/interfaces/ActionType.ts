export enum ActionType {
  edit = "edit",
  create = "create",
  delete = "delete",
}
