import { EntityType } from "./EntityType";

interface NoteType extends EntityType {
  dueDate: string;
  title: string;
  body: string;
}

export default NoteType;
