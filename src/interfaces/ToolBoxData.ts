import GoalType from "./GoalType";
import MedicationType from "./MedicationType";
import NoteType from "./NoteType";
import SymptomType from "./SymptomType";

export interface ToolBoxData {
  medications: MedicationType[];
  goals: GoalType[];
  symptoms: SymptomType[];
  notes: NoteType[];
  error?: string;
  msg?: string;
}
