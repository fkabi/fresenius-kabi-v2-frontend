import { EntityType } from "./EntityType";

interface SymptomType extends EntityType {
  name: string;
}

export default SymptomType;
