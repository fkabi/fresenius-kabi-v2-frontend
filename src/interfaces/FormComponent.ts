export enum FormComponent {
  Select = "Select",
  DatePicker = "DatePicker",
  DateTimePicker = "DateTimePicker",
  TextField = "TextField",
}
