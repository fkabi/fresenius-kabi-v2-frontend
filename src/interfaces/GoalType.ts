import { EntityType } from "./EntityType";

interface GoalType extends EntityType {
  status: string;
  date: string;
  name: string;
}

export default GoalType;
