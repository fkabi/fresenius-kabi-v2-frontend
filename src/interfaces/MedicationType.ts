import { EntityType } from "./EntityType";

interface MedicationType extends EntityType {
  status: string;
  frequency: number;
  startingDate: string;
  name: string;
}

export default MedicationType;
