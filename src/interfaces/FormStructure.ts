export interface FormStructure {
  components: Array<Object>;
  defaultValue: Object;
  editTitle: string;
  deleteTitle: string;
  createTitle: string;
}
