import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";

const useStyles = makeStyles((theme) => ({
  loader: {
    position: "absolute",
    left: "49%",
    top: "30%",
  },
}));

export default function WithLoader({ children, loading }) {
  const classes = useStyles();

  return (
    <>{loading ? <CircularProgress className={classes.loader} /> : children}</>
  );
}
