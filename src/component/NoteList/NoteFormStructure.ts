import { FormComponent } from "../../interfaces/FormComponent";
import { FormStructure } from "../../interfaces/FormStructure";

export const noteFormStructure = {
  components: [
    {
      component: FormComponent.TextField,
      label: "Title",
      name: "title",
      type: "text",
      size: 12,
    },
    {
      component: FormComponent.TextField,
      label: "Note",
      name: "body",
      type: "text",
      multiline: 4,
      size: 12,
    },
    {
      component: FormComponent.DatePicker,
      label: "Due date",
      name: "dueDate",
      disablePast: true,
      defaultVal: new Date().toISOString(),
      type: "date",
      size: 12,
    },
  ],
  createTitle: "Create note",
  editTitle: "Modify note",
  deleteTitle: "Are you sure you want to delete this Note?",
  defaultValue: {
    name: "",
    id: -1,
    dueDate: new Date().toISOString().replace("Z", ""),
    body: "",
  },
} as FormStructure;
