import React from "react";
import {
  createNote,
  deleteNote,
  getNotes,
  modifyNote,
} from "../../api/fetch/fetch";
import Entity from "../EntityList/Entity";
import { noteFormStructure } from "./NoteFormStructure";
import NoteList, { NoteListProps } from "./NoteList";

const Note = () => {
  return (
    <Entity
      createEntity={createNote}
      deleteEntity={deleteNote}
      modifyEntity={modifyNote}
      formStructure={noteFormStructure}
      getNewEntities={getNotes}
      maxNumber={1000}
      type="notes"
    >
      <NoteList {...({} as NoteListProps)} />
    </Entity>
  );
};

export default Note;
