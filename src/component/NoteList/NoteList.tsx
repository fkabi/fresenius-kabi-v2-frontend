import { Container, Paper, Tab, Tabs } from "@material-ui/core";
import { makeStyles, Theme } from "@material-ui/core/styles";
import { default as React, useEffect, useState } from "react";
import { FormStructure } from "../../interfaces/FormStructure";
import NoteType from "../../interfaces/NoteType";
import EntityList from "../EntityList/EntityList";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export interface NoteListProps {
  data: NoteType[];
  onCreate: (entity: Object) => void;
  onEdit: (changedEntity: Object) => void;
  onDelete: (id: number) => void;
  error: string;
  maxNumber: number;
  formStructure: FormStructure;
  noCreate: boolean;
}

const NoteList = (props: NoteListProps) => {
  const { data } = props;
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const [filteredEntities, setFilteredEntities] = useState<NoteType[]>(
    data.filter((e) => new Date(e.dueDate) > new Date())
  );

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  const filterEntities = () => {
    return value === 0
      ? data.filter((e) => new Date(e.dueDate) > new Date())
      : data.filter((e) => new Date(e.dueDate) <= new Date());
  };

  useEffect(() => {
    setFilteredEntities(filterEntities());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data, value]);

  return (
    <Container component="div" maxWidth="lg">
      <Paper className={classes.root}>
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          centered
        >
          <Tab label="Upcoming" />
          <Tab label="Past" />
        </Tabs>
      </Paper>
      <EntityList {...props} data={filteredEntities} />
    </Container>
  );
};

export default NoteList;
