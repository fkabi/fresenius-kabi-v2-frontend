import { IconButton, makeStyles } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import React from "react";
import { EntityType } from "../interfaces/EntityType";

const useStyles = makeStyles((theme) => ({
  inline: {
    display: "flex",
    alignItems: "center",
    flexWrap: "wrap",
  },
}));

export interface EditableListProps<T extends EntityType> {
  items: T[];
  renderItem: (entity: T) => JSX.Element;
  onEditClick: (entity: T) => void;
  onDeleteClick: (entity: T) => void;
}

const EditableList = <T extends EntityType>(props: EditableListProps<T>) => {
  const classes = useStyles();

  return (
    <>
      {props.items.map((item, index) => (
        <div className={classes.inline} key={item.id}>
          {props.renderItem(item)}
          <IconButton
            aria-label="edit"
            onClick={() => {
              props.onEditClick(item);
            }}
          >
            <EditIcon />
          </IconButton>
          <IconButton
            aria-label="delete"
            onClick={() => {
              props.onDeleteClick(item);
            }}
          >
            <DeleteIcon />
          </IconButton>
        </div>
      ))}
    </>
  );
};

export default EditableList;
