import { addDays } from "date-fns";
import { ActionType } from "../../interfaces/ActionType";
import { FormComponent } from "../../interfaces/FormComponent";
import { FormStructure } from "../../interfaces/FormStructure";

export const goalFormStructure = {
  components: [
    {
      component: FormComponent.TextField,
      label: "Name",
      name: "name",
      type: "text",
      size: 12,
    },
    {
      component: FormComponent.Select,
      label: "Status",
      type: "number",
      name: "status",
      values: ["0", "1", "2"],
      valueLabels: ["Active", "Completed - Achieved", "Completed - Failed"],
      size: 12,
    },
    {
      component: FormComponent.DatePicker,
      label: "Date",
      name: "date",
      allowedActions: [ActionType.create],
      disablePast: true,
      type: "date",
      size: 12,
    },
  ],
  createTitle: "Create goal",
  editTitle: "Modify goal",
  deleteTitle: "Are you sure you want to delete this Goal?",
  defaultValue: {
    name: "",
    id: -1,
    date: addDays(new Date(), 1).toISOString().substring(0, 10),
    status: "0",
  },
} as FormStructure;
