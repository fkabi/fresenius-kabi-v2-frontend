import React from "react";
import classes from "./GoalItem.module.css";

const GoalItem = (props) => {
  return (
    <div className={classes.GoalItem}>
      <div>{props.name}</div>
      <div>{props.status}</div>
      <div>{props.date}</div>
    </div>
  );
};

export default GoalItem;
