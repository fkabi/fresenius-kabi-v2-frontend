import { Container, Paper, Tab, Tabs } from "@material-ui/core";
import { makeStyles, Theme } from "@material-ui/core/styles";
import { default as React, useEffect, useState } from "react";
import { FormStructure } from "../../interfaces/FormStructure";
import GoalType from "../../interfaces/GoalType";
import EntityList from "../EntityList/EntityList";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export interface GoalListProps {
  data: GoalType[];
  onCreate: (entity: Object) => void;
  onEdit: (changedEntity: Object) => void;
  onDelete: (id: number) => void;
  error: string;
  maxNumber: number;
  formStructure: FormStructure;
  noCreate: boolean;
  type: "goals";
}

const GoalList = (props: GoalListProps) => {
  const { data } = props;
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const [filteredEntities, setFilteredEntities] = useState<GoalType[]>(
    data.filter((e) => e.status === "0")
  );

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  const filterEntities = () => {
    return value === 0
      ? data.filter((e) => e.status === "0" || e.status.toString() === "0")
      : data.filter((e) => e.status !== "0" || e.status.toString() !== "0");
  };

  useEffect(() => {
    setFilteredEntities(filterEntities());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data, value]);

  return (
    <Container component="div" maxWidth="lg">
      <Paper className={classes.root}>
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          centered
        >
          <Tab label="Active" />
          <Tab label="Completed" />
        </Tabs>
      </Paper>
      <EntityList {...props} data={filteredEntities} />
    </Container>
  );
};

export default GoalList;
