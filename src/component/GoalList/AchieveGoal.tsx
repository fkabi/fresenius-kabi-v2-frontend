import {
  Button,
  Container,
  FormHelperText,
  Grid,
  Typography,
} from "@material-ui/core";
import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { Api } from "../../api/fetch/Api";
import { useDispatch, useSelector } from "react-redux";
import { TOOLBOX_DATA_UPDATE } from "../../store/ToolBoxActions";
import { toolBoxState } from "../../store/Reducer";
import { ToolBoxData } from "../../interfaces/ToolBoxData";

const AchieveGoal = () => {
  let { id } = useParams();
  const [error, setError] = useState("");
  const [msg, setMsg] = useState("");
  const dispatch = useDispatch();
  const toolbox: ToolBoxData = useSelector(toolBoxState);

  const saveLevel = () => {
    Api.modifyGoal({
      field_goal_status: [
        {
          target_id: 1,
        },
      ],
      id: [
        {
          value: id,
        },
      ],
    })
      .then((response) => {
        const newGoals = toolbox.goals.map((g) =>
          g.id === id ? { ...g, status: 1 } : g
        );
        dispatch({
          type: TOOLBOX_DATA_UPDATE,
          data: { goals: newGoals },
        });
        setMsg(response.message);
      })
      .catch((error) => setError(error));
  };

  return (
    <Container component="main" maxWidth="sm">
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Typography variant="h5" component="h5">
            Do you want to complete this goal?
          </Typography>
        </Grid>
      </Grid>

      <FormHelperText id="component-helper-text" error>
        {error}
      </FormHelperText>
      <FormHelperText id="component-helper-text">{msg}</FormHelperText>
      <Button
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
        onClick={saveLevel}
      >
        Complete
      </Button>
    </Container>
  );
};

export default AchieveGoal;
