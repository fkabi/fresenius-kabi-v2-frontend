import { Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { Api } from "../../api/fetch/Api";
import MedicationType from "../../interfaces/MedicationType";
import { ToolBoxData } from "../../interfaces/ToolBoxData";
import { loadingState, toolBoxState } from "../../store/Reducer";
import EntityItem from "../EntityList/EntityItem";
import WithLoader from "../WithLoader";

function sameDay(d1, d2) {
  return (
    d1.getFullYear() === d2.getFullYear() &&
    d1.getMonth() === d2.getMonth() &&
    d1.getDate() === d2.getDate()
  );
}

const CheckIn = () => {
  const { medications, goals, symptoms }: ToolBoxData = useSelector(
    toolBoxState
  );
  const [medReports, setMedReports] = useState<MedicationType[]>([]);
  const loading = useSelector(loadingState);
  const history = useHistory();

  useEffect(() => {
    Api.getUserMedicationReport()
      .then((resp) => {
        const newVals = Object.values(
          resp.data.content as MedicationType
        ).filter((med) => sameDay(new Date(med.date), new Date()));
        setMedReports([...medReports, ...newVals]);
      })
      .catch((error) => console.log("error", error.message));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleClick = (id: number) => {
    history.push("symptom/" + id);
  };

  const handleMedClick = (id: number) => {
    history.push("medication/" + id);
  };

  const handleGoalClick = (id: number) => {
    history.push("goal/" + id);
  };

  return (
    <>
      <WithLoader loading={loading}>
        <Typography variant="h4" component="h4">
          Goals
        </Typography>
        {goals
          .filter((g) => g.status === "0")
          .map((goal, i) => (
            <div key={i} onClick={() => handleGoalClick(goal.id)}>
              <EntityItem entity={goal} />
            </div>
          ))}

        <Typography variant="h4" component="h4">
          Medications
        </Typography>
        {medications.map((med, i) => (
          <div key={i} onClick={() => handleMedClick(med.id)}>
            <EntityItem entity={med} />
          </div>
        ))}

        <Typography variant="h4" component="h4">
          Symptoms
        </Typography>
        {symptoms.map((sym, i) => (
          <div key={i} onClick={() => handleClick(sym.id)}>
            <EntityItem entity={sym} />
            {sym.id}
          </div>
        ))}
      </WithLoader>
    </>
  );
};

export default CheckIn;
