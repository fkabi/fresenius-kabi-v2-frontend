import { FormComponent } from "../../interfaces/FormComponent";
import { FormStructure } from "../../interfaces/FormStructure";

export const symptomFormStructure = {
  components: [
    {
      component: FormComponent.TextField,
      label: "Name",
      name: "name",
      type: "text",
      defaultVal: "",
      size: 12,
    },
  ],
  createTitle: "Create your own",
  editTitle: "Modify medication",
  deleteTitle: "Are you sure you want to delete this Medication?",
  defaultValue: { name: "", id: -1 },
} as FormStructure;
