import {
  Button,
  Container,
  FormHelperText,
  Grid,
  Typography,
} from "@material-ui/core";
import SentimentDissatisfiedIcon from "@material-ui/icons/SentimentDissatisfied";
import SentimentSatisfiedIcon from "@material-ui/icons/SentimentSatisfied";
import SentimentSatisfiedAltIcon from "@material-ui/icons/SentimentSatisfiedAlt";
import SentimentVeryDissatisfiedIcon from "@material-ui/icons/SentimentVeryDissatisfied";
import SentimentVerySatisfiedIcon from "@material-ui/icons/SentimentVerySatisfied";
import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { Api } from "../../api/fetch/Api";

const DailySymptom = () => {
  let { id } = useParams();
  const [level, setLevel] = useState(1);
  const [error, setError] = useState("");
  const [msg, setMsg] = useState("");

  const handleClick = (level: number) => {
    setLevel(level);
  };

  const saveLevel = () => {
    Api.createDailyUserSymptom({
      field_daily_symptom_symptom_ref: [
        {
          target_id: id,
        },
      ],
      field_daily_symptom_value: [
        {
          value: level,
        },
      ],
    })
      .then((resp) => setMsg(resp.message))
      .catch((error) => setError(error.message));
  };

  const getLevel = () => {
    switch (level) {
      case 1:
        return "Very sad";
      case 2:
        return "Sad";
      case 3:
        return "Average";
      case 4:
        return "Happy";
      case 5:
        return "Very Happy";
    }
  };

  return (
    <Container component="main" maxWidth="sm">
      <Grid container spacing={3}>
        <Grid item xs={3} sm={3}>
          <SentimentVeryDissatisfiedIcon onClick={() => handleClick(1)} />
          <SentimentDissatisfiedIcon onClick={() => handleClick(2)} />
          <SentimentSatisfiedIcon onClick={() => handleClick(3)} />
          <SentimentSatisfiedAltIcon onClick={() => handleClick(4)} />
          <SentimentVerySatisfiedIcon onClick={() => handleClick(5)} />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h5" component="h5">
            {getLevel()}
          </Typography>
        </Grid>
      </Grid>
      <FormHelperText id="component-helper-text">{msg}</FormHelperText>
      <FormHelperText id="component-helper-text" error>
        {error}
      </FormHelperText>
      <Button
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
        onClick={saveLevel}
      >
        Save
      </Button>
    </Container>
  );
};

export default DailySymptom;
