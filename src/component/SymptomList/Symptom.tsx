import React from "react";
import {
  createSymptom,
  deleteSymptom,
  getSymptoms,
  modifySymptom,
} from "../../api/fetch/fetch";
import Entity from "../EntityList/Entity";
import { symptomFormStructure } from "./SymptomFormStructure";

const Symptom = () => {
  return (
    <Entity
      createEntity={createSymptom}
      deleteEntity={deleteSymptom}
      modifyEntity={modifySymptom}
      formStructure={symptomFormStructure}
      getNewEntities={getSymptoms}
      maxNumber={1000}
      type="symptoms"
    />
  );
};

export default Symptom;
