import _ from "lodash";
import React, { cloneElement, ReactElement, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { EntityType } from "../../interfaces/EntityType";
import { FormStructure } from "../../interfaces/FormStructure";
import { ToolBoxData } from "../../interfaces/ToolBoxData";
import { loadingState, toolBoxState } from "../../store/Reducer";
import { TOOLBOX_DATA_UPDATE } from "../../store/ToolBoxActions";
import { fromEntityData } from "../../util/mapDataToEntityFormat";
import WithLoader from "../WithLoader";
import EntityList, { EntityListProps } from "./EntityList";

interface EntityProps {
  getNewEntities: (successFn: Function, errorFn: Function) => void;
  createEntity: (data: Object, successFn: Function, errorFn: Function) => void;
  modifyEntity: (data: Object, successFn: Function, errorFn: Function) => void;
  deleteEntity: (id: number, successFn: Function, errorFn: Function) => void;
  maxNumber: number;
  formStructure: FormStructure;
  renderList?: JSX.Element;
  children?: ReactElement;
  type: string;
}

const Entity = <E extends EntityType>(props: EntityProps) => {
  const loading = useSelector(loadingState);
  const [error, setError] = useState("");
  const dispatch = useDispatch();
  const toolbox: ToolBoxData = useSelector(toolBoxState);

  const data: E[] = toolbox[props.type] || [];

  const onCreate = (entity: Object) => {
    if (!entity) {
      return;
    }

    props.createEntity(
      entity,
      (resp) => {
        if (!resp.success) {
          setError(resp.message);
        } else {
          dispatch({
            type: TOOLBOX_DATA_UPDATE,
            data: {
              [props.type]: [
                ...data,
                ...(Object.values(resp.data.content) as E[]),
              ],
            },
          });
        }
      },
      (error) => {
        setError(error);
      }
    );
  };

  const onEdit = (entity) => {
    props.modifyEntity(
      entity,
      (resp) => {},
      (error) => {
        setError(error);
      }
    );

    const newData = data.map((s) =>
      s.id === entity.id[0].value
        ? Object.assign({}, s, fromEntityData(entity))
        : s
    );
    dispatch({ type: TOOLBOX_DATA_UPDATE, data: { [props.type]: newData } });
  };

  const onDelete = (id: number) => {
    props.deleteEntity(
      id,
      () => {},
      (error) => {
        setError(error);
      }
    );

    dispatch({
      type: TOOLBOX_DATA_UPDATE,
      data: { [props.type]: _.remove(data, (s) => s.id !== id) },
    });
  };

  const { formStructure, maxNumber, children } = props;

  const childProps = {
    data,
    onCreate,
    onEdit,
    onDelete,
    error,
    maxNumber,
    formStructure,
  } as EntityListProps<E>;

  return (
    <WithLoader loading={loading}>
      {children
        ? cloneElement(children, { ...childProps })
        : data.length !== 0 && !children && <EntityList {...childProps} />}
    </WithLoader>
  );
};

export default Entity;
