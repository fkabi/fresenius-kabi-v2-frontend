import { Button, Container, FormHelperText, Grid } from "@material-ui/core";
import React, { useState } from "react";
import { ActionType } from "../../interfaces/ActionType";
import { EntityType } from "../../interfaces/EntityType";
import { FormStructure } from "../../interfaces/FormStructure";
import { toEntityData } from "../../util/mapDataToEntityFormat";
import Dialog from "../Dialog";
import EditableList from "../EditableList";
import DynamicForm from "./DynamicForm";
import EntityItem from "./EntityItem";

export interface EntityListProps<E extends EntityType> {
  data: E[];
  onCreate: (entity: Object) => void;
  onEdit: (changedEntity: Object) => void;
  onDelete: (id: number) => void;
  error: string;
  maxNumber: number;
  formStructure: FormStructure;
  noCreate: boolean;
}

const EntityList = <E extends EntityType>(props: EntityListProps<E>) => {
  const [newEntity, setNewEntity] = useState<E>(
    (props.formStructure.defaultValue as unknown) as E
  );
  const [changedEntity, setChangedEntity] = useState<E>();
  const [dialogOpen, setDialogOpen] = useState(false);
  const [action, setAction] = useState<ActionType>(ActionType.create);

  const getDialogData = () => {
    switch (action) {
      case ActionType.create:
        return {
          onButton1: () => {
            if (newEntity) {
              setDialogOpen(false);
              props.onCreate(toEntityData(newEntity));
            }
          },
          button1Text: "Create",
          button2Text: "Close",
          title: props.formStructure.createTitle,
        };

      case ActionType.edit:
        return {
          onButton1: () => {
            if (changedEntity) {
              setDialogOpen(false);
              props.onEdit(toEntityData({ ...changedEntity, ...newEntity }));
            }
          },
          button1Text: "Edit",
          button2Text: "Close",
          title: props.formStructure.editTitle,
        };

      case ActionType.delete:
        return {
          onButton1: () => {
            if (changedEntity) {
              setDialogOpen(false);
              props.onDelete(changedEntity.id);
            }
          },
          button1Text: "Delete",
          button2Text: "Close",
          title: props.formStructure.deleteTitle,
          renderContent: undefined,
        };
    }
  };

  return (
    <Container component="div" maxWidth="md">
      <EditableList
        items={props.data}
        onEditClick={(e) => {
          setChangedEntity(e);
          setNewEntity({ ...e });
          setDialogOpen(true);
          setAction(ActionType.edit);
        }}
        onDeleteClick={(e) => {
          setChangedEntity(e);
          setDialogOpen(true);
          setAction(ActionType.delete);
        }}
        renderItem={(e) => <EntityItem entity={e} />}
      />
      {!props.noCreate && (
        <>
          <Grid container spacing={2}>
            <Grid item xs={4}>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                onClick={() => {
                  setAction(ActionType.create);
                  setDialogOpen(true);
                  setNewEntity(
                    (props.formStructure.defaultValue as unknown) as E
                  );
                }}
                disabled={props.data.length >= props.maxNumber}
              >
                {props.formStructure.createTitle}
              </Button>
            </Grid>
          </Grid>
          {props.data.length >= props.maxNumber && (
            <FormHelperText id="limit-text">Reached limit</FormHelperText>
          )}
          <FormHelperText id="component-helper-text" error>
            {props.error}{" "}
          </FormHelperText>
        </>
      )}
      <Dialog
        open={dialogOpen}
        onButton2={() => setDialogOpen(false)}
        renderContent={
          newEntity && (
            <DynamicForm
              entity={newEntity}
              formStructure={props.formStructure.components}
              onChange={(e) => setNewEntity(Object.assign({}, newEntity, e))}
              actionType={action}
            />
          )
        }
        {...getDialogData()}
      />
    </Container>
  );
};

export default EntityList;
