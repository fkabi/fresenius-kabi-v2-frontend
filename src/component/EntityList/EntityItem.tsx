import { Typography } from "@material-ui/core";
import React from "react";
import { EntityType } from "../../interfaces/EntityType";

interface EntityItemProps<E> {
  entity: E;
}

const EntityItem = <E extends EntityType>(props: EntityItemProps<E>) => {
  const { entity } = props;

  return (
    <>
      {Object.values(entity).map((val, index) => (
        <Typography
          variant="body1"
          component="p"
          display={"inline"}
          key={props.entity.id + val + index}
        >
          {val}&nbsp;
        </Typography>
      ))}
    </>
  );
};

export default EntityItem;
