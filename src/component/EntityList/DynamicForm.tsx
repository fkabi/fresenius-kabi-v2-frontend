import DateFnsUtils from "@date-io/date-fns";
import {
  Container,
  Grid,
  makeStyles,
  MenuItem,
  TextField,
} from "@material-ui/core";
import {
  DateTimePicker,
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import React, { Fragment } from "react";
import { ActionType } from "../../interfaces/ActionType";
import { EntityType } from "../../interfaces/EntityType";
import { FormComponent } from "../../interfaces/FormComponent";

const useStyles = makeStyles((theme) => ({
  form: {
    width: "100%",
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  formControl: {
    width: "100%",
    minWidth: 80,
  },
  control: {
    width: "100%",
    marginTop: "16px",
  },
}));

interface DynamicFormProps<E extends EntityType> {
  entity: E;
  formStructure: Array<Object>;
  onChange: (data: Object) => void;
  actionType: ActionType;
}

const DynamicForm = <E extends EntityType>(props: DynamicFormProps<E>) => {
  const classes = useStyles();

  const renderField = (field) => {
    if (
      field.allowedActions &&
      !field.allowedActions.includes(props.actionType)
    ) {
      return <></>;
    }

    switch (field.component) {
      case FormComponent.TextField:
        return (
          <Grid item xs={field.size} key={field.name}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              id={"textfield_" + field.name}
              label={field.label}
              multiline={field.multiline ? true : false}
              rows={field.multiline}
              autoFocus
              onChange={(event) =>
                props.onChange({ [field.name]: event.target.value })
              }
              value={props.entity[field.name]}
              className={classes.control}
            />
          </Grid>
        );
      case FormComponent.Select:
        return (
          <Grid item xs={field.size} key={field.name}>
            <TextField
              id={"select_" + field.name}
              select
              label={field.label}
              variant="outlined"
              margin="normal"
              value={props.entity[field.name]}
              onChange={(event) =>
                props.onChange({ [field.name]: event.target.value })
              }
              className={classes.control}
            >
              {field.values.map((val, index) => (
                <MenuItem value={val} key={val}>
                  {field.valueLabels ? field.valueLabels[index] : val}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
        );

      case FormComponent.DatePicker:
        return (
          <Grid item xs={field.size} key={field.name}>
            <KeyboardDatePicker
              disableToolbar
              variant="inline"
              inputVariant="outlined"
              format="dd/MM/yyyy"
              margin="normal"
              id={"date_picker_" + field.name}
              label={field.label}
              disablePast={field.disablePast}
              value={new Date(props.entity[field.name])}
              onChange={(date) =>
                props.onChange({
                  [field.name]: date?.toISOString().substring(0, 10),
                })
              }
              KeyboardButtonProps={{
                "aria-label": "change date",
              }}
              className={classes.control}
            />
          </Grid>
        );

      case FormComponent.DateTimePicker:
        return (
          <Grid item xs={field.size} key={field.name}>
            <Fragment>
              <DateTimePicker
                disableToolbar
                inputVariant="outlined"
                margin="normal"
                id={"date_time_picker_" + field.name}
                label={field.label}
                disablePast={field.disablePast}
                value={new Date(props.entity[field.name] + "Z")}
                onChange={(date) =>
                  props.onChange({
                    [field.name]: date?.toISOString().replace("Z", ""),
                  })
                }
                className={classes.control}
              />
            </Fragment>
          </Grid>
        );
    }
  };

  return (
    <Container component="div" maxWidth="md">
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            {props.formStructure.map((field) => renderField(field))}
          </Grid>
        </form>
      </MuiPickersUtilsProvider>
    </Container>
  );
};

export default DynamicForm;
