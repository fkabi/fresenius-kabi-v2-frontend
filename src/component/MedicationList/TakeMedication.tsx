import {
  Button,
  Container,
  FormHelperText,
  Grid,
  Typography,
} from "@material-ui/core";
import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { Api } from "../../api/fetch/Api";

const TakeMedication = () => {
  let { id } = useParams();
  const [error, setError] = useState("");
  const [msg, setMsg] = useState("");

  const saveLevel = () => {
    Api.createMedicationReport({
      field_usermedreport_med_ref: [
        {
          target_id: id,
        },
      ],
    })
      .then((resp) => setMsg(resp.message))
      .catch((error) => setError(error.data.content));
  };

  return (
    <Container component="main" maxWidth="sm">
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Typography variant="h5" component="h5">
            Do you want to take this medication?
          </Typography>
        </Grid>
      </Grid>

      <FormHelperText id="component-helper-text" error>
        {error}
      </FormHelperText>
      <FormHelperText id="component-helper-text">{msg}</FormHelperText>
      <Button
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
        onClick={saveLevel}
      >
        Take
      </Button>
    </Container>
  );
};

export default TakeMedication;
