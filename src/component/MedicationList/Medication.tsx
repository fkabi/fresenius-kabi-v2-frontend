import React from "react";
import {
  createMedication,
  deleteMedication,
  getMedications,
  modifyMedication,
} from "../../api/fetch/fetch";
import Entity from "../EntityList/Entity";
import { medicationFormStructure } from "./MedicationFormStructure";

const Medication = () => {
  return (
    <Entity
      createEntity={createMedication}
      deleteEntity={deleteMedication}
      modifyEntity={modifyMedication}
      formStructure={medicationFormStructure}
      getNewEntities={getMedications}
      maxNumber={5}
      type="medications"
    />
  );
};

export default Medication;
