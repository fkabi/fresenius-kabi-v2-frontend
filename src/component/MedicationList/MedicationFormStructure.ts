import _ from "lodash";
import { FormComponent } from "../../interfaces/FormComponent";
import { FormStructure } from "../../interfaces/FormStructure";

export const medicationFormStructure = {
  components: [
    {
      component: FormComponent.TextField,
      label: "Name",
      name: "name",
      type: "text",
      size: 12,
    },
    {
      component: FormComponent.Select,
      label: "Status",
      type: "number",
      name: "status",
      values: ["0", "1"],
      valueLabels: ["Inactive", "Active"],
      valueLabel: "statusName",
      size: 12,
    },
    {
      component: FormComponent.Select,
      label: "Frequency",
      type: "number",
      name: "frequency",
      values: _.range(1, 32),
      size: 12,
    },
    {
      component: FormComponent.DatePicker,
      label: "Starting date",
      name: "startingDate",
      disablePast: true,
      defaultVal: new Date().toISOString(),
      type: "date",
      size: 12,
    },
  ],
  createTitle: "Create medication",
  editTitle: "Modify medication",
  deleteTitle: "Are you sure you want to delete this Medication?",
  defaultValue: {
    name: "",
    frequency: 1,
    id: -1,
    startingDate: new Date().toISOString(),
    status: "0",
  },
} as FormStructure;
