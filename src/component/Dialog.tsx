import { Container } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import MuiDialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import React from "react";

export interface DialogProps {
  title?: string;
  button1Text?: string;
  button2Text?: string;
  contentText?: string;
  renderContent?: JSX.Element;
  onButton1?: () => void;
  onButton2?: () => void;
  open: boolean;
}

export default function Dialog({
  title,
  button1Text,
  button2Text,
  contentText,
  renderContent,
  onButton1,
  onButton2,
  open,
}: DialogProps) {
  return (
    <Container component="div" maxWidth="md">
      <MuiDialog
        open={open}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        maxWidth="md"
      >
        <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
        <DialogContent>
          {contentText && (
            <DialogContentText id="alert-dialog-description">
              {contentText}
            </DialogContentText>
          )}
          {renderContent}
        </DialogContent>
        <DialogActions>
          {button1Text && (
            <Button onClick={onButton1} color="primary">
              {button1Text}
            </Button>
          )}
          {button2Text && (
            <Button onClick={onButton2} color="primary" autoFocus>
              {button2Text}
            </Button>
          )}
        </DialogActions>
      </MuiDialog>
    </Container>
  );
}
