import {
  FormControl,
  InputLabel,
  makeStyles,
  MenuItem,
  Select,
} from "@material-ui/core";
import _ from "lodash";
import React, { useEffect, useState } from "react";
import { getSymptoms, getUserDailySymptom } from "../../api/fetch/fetch";
import WithLoader from "../WithLoader";
import SymptomTrackerGraph from "./SymptomTrackerGraph";
import {
  GraphData,
  Interval,
  Symptom,
  SymptomDailyData,
} from "./SymptomTrackerInterface";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const DailySymptomTracker = () => {
  const classes = useStyles();
  const [interval, setInterval] = useState<Interval>(Interval.month);
  const [loading, setLoading] = useState(true);
  const [graphData, setGraphData] = useState<GraphData[]>([]);
  const [data, setData] = useState<Array<SymptomDailyData>>([]);
  const [symptom, setSymptom] = useState<Symptom>();
  const [symptoms, setSymptoms] = useState<Array<Symptom>>([]);

  const handleIntervalChange = (
    event: React.ChangeEvent<{ value: unknown }>
  ) => {
    setLoading(true);
    const newInterval = event.target.value as Interval;
    setInterval(newInterval);
  };

  const handleSymptomChange = (
    event: React.ChangeEvent<{ value: unknown }>
  ) => {
    const newSymptomId = event.target.value as number;
    const found = symptoms.find((s) => s.id == newSymptomId);
    setSymptom(found);
  };

  const updateGraphData = (
    symptomId = symptom?.id,
    newData = data,
    newInterval: Interval = interval
  ) => {
    setGraphData(
      newData
        .filter((d) => d.symptom_id == symptomId)
        .map((d) => {
          return { y: d.value, x: d.date };
        })
    );
  };

  const getUserDailySymptomForInterval = () => {
    getUserDailySymptom(
      interval,
      (respData) => {
        let newData = _.keys(respData.data.content).map(
          (c) => respData.data.content[c]
        );

        if (newData[0] == "[]") {
          newData = [];
        }
        setData(newData);
        setLoading(false);
      },
      (error) => {
        console.log("error");
        setLoading(false);
      }
    );
  };

  useEffect(() => {
    getSymptoms(
      (response) => {
        const symptoms = response.data.content;
        const mappedSymptoms = _.keys(symptoms).map((id) => symptoms[id]);
        setSymptoms(mappedSymptoms);
        setSymptom(mappedSymptoms[0]);
        setInterval(Interval.month);
        setLoading(false);
      },
      (error) => {
        setLoading(false);
        console.log("error", error);
      }
    );
  }, []);

  useEffect(() => {
    updateGraphData();
  }, [symptom, data]);

  useEffect(() => {
    getUserDailySymptomForInterval();
  }, [interval]);

  return (
    <>
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-label"></InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={interval}
          onChange={handleIntervalChange}
        >
          <MenuItem value={Interval.month}>Month</MenuItem>
          <MenuItem value={Interval.week}>Week</MenuItem>
          <MenuItem value={Interval.day}>Day</MenuItem>
        </Select>
      </FormControl>

      {symptom && (
        <FormControl className={classes.formControl}>
          <InputLabel id="demo-simple-select-label"></InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={symptom.id}
            onChange={handleSymptomChange}
          >
            {symptoms.map((s) => (
              <MenuItem key={s.id} value={s.id}>
                {s.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      )}

      <WithLoader loading={loading}>
        <SymptomTrackerGraph interval={interval} data={graphData} />
      </WithLoader>
    </>
  );
};

export default DailySymptomTracker;
