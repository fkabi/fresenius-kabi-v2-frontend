export interface GraphData {
  x: Date | string;
  y?: 1 | 2 | 3 | 4 | 5 | null;
}

export interface SymptomTrackerGraphProps {
  interval: Interval;
  data: GraphData[];
}

export enum Interval {
  month = "month",
  week = "week",
  day = "day",
}

export interface Symptom {
  id: number;
  name: string;
}

export interface SymptomDailyData {
  id: number;
  symptom_name: string;
  symptom_id: number;
  date: string;
  value: 1 | 2 | 3 | 4 | 5;
}
