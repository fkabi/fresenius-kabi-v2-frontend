import { makeStyles } from "@material-ui/core";
import SentimentDissatisfiedIcon from "@material-ui/icons/SentimentDissatisfied";
import SentimentSatisfiedIcon from "@material-ui/icons/SentimentSatisfied";
import SentimentSatisfiedAltIcon from "@material-ui/icons/SentimentSatisfiedAlt";
import SentimentVeryDissatisfiedIcon from "@material-ui/icons/SentimentVeryDissatisfied";
import SentimentVerySatisfiedIcon from "@material-ui/icons/SentimentVerySatisfied";
import { ResponsiveLine } from "@nivo/line";
import { SymptomTrackerGraphProps } from "./SymptomTrackerInterface";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "600px",
  },
}));

const SymptomTrackerGraph = ({
  interval,
  data = [],
}: SymptomTrackerGraphProps) => {
  const classes = useStyles();

  const getTick = (tick) => {
    return (
      <foreignObject
        width={60}
        height={30}
        transform={`translate(${tick.x - 60},${tick.y - 20})`}
      >
        <div className="customLeftAxisTick">{getSvg(tick)}</div>
      </foreignObject>
    );
  };

  const getSvg = (tick) => {
    switch (tick.value) {
      case 1:
        return <SentimentVeryDissatisfiedIcon />;
      case 2:
        return <SentimentDissatisfiedIcon />;
      case 3:
        return <SentimentSatisfiedIcon />;
      case 4:
        return <SentimentSatisfiedAltIcon />;
      default:
        return <SentimentVerySatisfiedIcon />;
    }
  };

  const graphData = [
    {
      id: "mood",
      color: "hsl(180, 70%, 50%)",
      data: data,
    },
  ];

  return (
    <div className={classes.root}>
      <ResponsiveLine
        data={graphData}
        margin={{ top: 50, right: 110, bottom: 50, left: 70 }}
        xScale={{
          type: "time",
          format: "%Y-%m-%d",
          useUTC: false,
          precision: "day",
        }}
        xFormat="time:%Y-%m-%d"
        yScale={{
          type: "linear",
          min: 1,
          max: 5,
          stacked: true,
          reverse: false,
        }}
        curve={"monotoneX"}
        areaBaselineValue={1}
        axisTop={null}
        axisRight={null}
        axisBottom={{
          format: "%b %d",
          tickValues: "every 1 day",
          legend: "Date",
          legendOffset: -12,
        }}
        axisLeft={{
          orient: "left",
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          tickValues: [1, 2, 3, 4, 5],
          renderTick: getTick,
          legendPosition: "middle",
        }}
        colors={{ scheme: "paired" }}
        pointSize={10}
        pointColor={{ theme: "background" }}
        pointBorderWidth={2}
        pointBorderColor={{ from: "serieColor" }}
        enableArea={true}
        enableGridX={false}
        legends={[
          {
            anchor: "bottom-right",
            direction: "column",
            justify: false,
            translateX: 100,
            translateY: 0,
            itemsSpacing: 0,
            itemDirection: "left-to-right",
            itemWidth: 80,
            itemHeight: 20,
            itemOpacity: 0.75,
            symbolSize: 12,
            symbolShape: "circle",
            symbolBorderColor: "rgba(0, 0, 0, .5)",
            effects: [
              {
                on: "hover",
                style: {
                  itemBackground: "rgba(0, 0, 0, .03)",
                  itemOpacity: 1,
                },
              },
            ],
          },
        ]}
      />
    </div>
  );
};

export default SymptomTrackerGraph;
