import { Link } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import { authState } from "../store/Reducer";

interface PrivateLinkProps {
  path: string;
  href?: string;
  variant:
    | "inherit"
    | "button"
    | "overline"
    | "caption"
    | "h1"
    | "h2"
    | "h3"
    | "h4"
    | "h5"
    | "h6"
    | "subtitle1"
    | "subtitle2"
    | "body1"
    | "body2"
    | "srOnly"
    | undefined;
  children: React.ReactChildren | React.ReactChild;
}

function PrivateLink({ href, variant, children }: PrivateLinkProps) {
  const { authenticated } = useSelector(authState);

  return authenticated ? (
    <Link href={href} variant={variant}>
      {children}
    </Link>
  ) : (
    <></>
  );
}

export default PrivateLink;
