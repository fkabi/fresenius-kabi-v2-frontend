import React from "react";
import { useSelector } from "react-redux";
import { Route, useHistory } from "react-router-dom";
import { authState } from "../store/Reducer";

interface PrivateRouteProps {
  path: string;
  children: React.ReactChildren | React.ReactChild;
}

function PrivateRoute(props: PrivateRouteProps) {
  const { authenticated } = useSelector(authState);
  const history = useHistory();

  return (
    <Route path={props.path}>
      {authenticated ? props.children : history.push("/login")}
    </Route>
  );
}

export default PrivateRoute;
