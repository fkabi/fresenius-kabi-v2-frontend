import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AppBar from "../component/AppBar";
import CheckIn from "../component/CheckIn/CheckIn";
import DailySymptomTracker from "../component/DailySymptomTracker/DailySymptomTracker";
import AchieveGoal from "../component/GoalList/AchieveGoal";
import Medication from "../component/MedicationList/Medication";
import TakeMedication from "../component/MedicationList/TakeMedication";
import Note from "../component/NoteList/Note";
import DailySymptom from "../component/SymptomList/DailySymptom";
import Symptom from "../component/SymptomList/Symptom";
import Goal from "../containers/Goal";
import { getUserData } from "../context/AuthContext";
import { CoolbagController } from "../coolbag/CoolbagController";
import CoolbagForm from "../coolbag/CoolbagForm";
import CoolbagThankYou from "../coolbag/CoolbagThankYou";
import { LoginController } from "../login/LoginController";
import Login from "../login/LoginForm";
import { RegisterController } from "../register/RegisterController";
import RegisterFormHCP from "../register/RegisterFormHCP";
import RegisterFormPatient from "../register/RegisterFormPatient";
import { LOGIN } from "../store/AuthActions";
import { authState } from "../store/Reducer";
import {
  LOADING_STARTED,
  TOOLBOX_FETCH_REQUESTED,
} from "../store/ToolBoxActions";
import PrivateRoute from "./PrivateRoute";

export const Routes: React.FC = () => {
  const auth = useSelector(authState);

  const dispatch = useDispatch();
  const userData = getUserData();

  useEffect(() => {
    if (userData.access_token && auth.authenticated === false) {
      dispatch({ type: LOGIN, userData: userData });
      dispatch({ type: TOOLBOX_FETCH_REQUESTED });
      dispatch({ type: LOADING_STARTED });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Router>
      <div>
        {/* A <Switch> looks through its children <Route>s and
              renders the first one that matches the current URL. */}
        <AppBar auth={auth} />
        <Switch>
          <Route path="/about">
            <></>
          </Route>
          <Route path="/login">
            <LoginController>
              <Login />
            </LoginController>
          </Route>
          <Route path="/hcp/register">
            <RegisterController>
              <RegisterFormHCP />
            </RegisterController>
          </Route>
          <Route path="/patient/register">
            <RegisterController>
              <RegisterFormPatient />
            </RegisterController>
          </Route>
          <Route path="/symptom-tracker">
            <DailySymptomTracker />
          </Route>
          <PrivateRoute path="/coolbag">
            <CoolbagController>
              <CoolbagForm />
            </CoolbagController>
          </PrivateRoute>
          <PrivateRoute path="/thankyou">
            <CoolbagThankYou />
          </PrivateRoute>
          <PrivateRoute path="/patient/medication">
            <Medication />
          </PrivateRoute>
          <PrivateRoute path="/patient/goal">
            <Goal />
          </PrivateRoute>
          <PrivateRoute path="/patient/note">
            <Note />
          </PrivateRoute>
          <PrivateRoute path="/patient/symptom">
            <Symptom />
          </PrivateRoute>
          <PrivateRoute path="/check-in">
            <CheckIn />
          </PrivateRoute>
          <PrivateRoute path="/symptom/:id">
            <DailySymptom />
          </PrivateRoute>
          <PrivateRoute path="/medication/:id">
            <TakeMedication />
          </PrivateRoute>
          <PrivateRoute path="/goal/:id">
            <AchieveGoal />
          </PrivateRoute>
          <Route path="/">
            <></>
          </Route>
        </Switch>
      </div>
    </Router>
  );
};
