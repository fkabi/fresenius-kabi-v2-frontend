export interface AuthContext {
  token?: string;
  logoutToken?: string;
  user?: object;
  authenticated?: boolean;
  tokenType?: string;
  expiresIn?: number;
  accessToken?: string;
  refreshToken?: string;
  msg?: string;
}

export interface UserLoginData {
  csrf_token: string;
  current_user: {
    uid: string;
    name: string;
  };
  logout_token: string;
  token_type: string;
  expires_in: number;
  access_token: string;
  refresh_token: string;
}

export function getUserData(): UserLoginData {
  const tokenString = sessionStorage.getItem("token");

  const userData = tokenString ? JSON.parse(tokenString) : {};
  return userData;
}
