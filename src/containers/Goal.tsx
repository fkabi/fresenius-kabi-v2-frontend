import {
  createGoal,
  deleteGoal,
  getGoals,
  modifyGoal,
} from "../api/fetch/fetch";
import Entity from "../component/EntityList/Entity";
import { goalFormStructure } from "../component/GoalList/GoalFormStructure";
import GoalList, { GoalListProps } from "../component/GoalList/GoalList";

const Goal = () => {
  return (
    <Entity
      createEntity={createGoal}
      deleteEntity={deleteGoal}
      modifyEntity={modifyGoal}
      formStructure={goalFormStructure}
      getNewEntities={getGoals}
      maxNumber={1000}
      type="goals"
    >
      <GoalList {...({} as GoalListProps)} />
    </Entity>
  );
};

export default Goal;
