import { EntityType } from "../interfaces/EntityType";
import GoalType from "../interfaces/GoalType";
import MedicationType from "../interfaces/MedicationType";
import NoteType from "../interfaces/NoteType";
import SymptomType from "../interfaces/SymptomType";

const mapDataToEntityFormat = (data: Object): Object => {
  const arrayData = Object.values(data);
  const keys = Object.keys(data);

  return arrayData.reduce((result: any, value: string, i: number) => {
    const currentKey = keys[i];
    const index = currentKey.includes("ref") ? "target_id" : "value";

    result[currentKey] = [{ [index]: value }];

    return result;
  }, {});
};

export const toEntityData = <T extends EntityType>(entity: T): Object => {
  const med = (entity as unknown) as MedicationType;
  const sym = (entity as unknown) as SymptomType;
  const goal = (entity as unknown) as GoalType;
  const note = (entity as unknown) as NoteType;

  if (med.frequency && med.startingDate && med.name) {
    return mapDataToEntityFormat({
      field_medication_label: med.name,
      field_medication_frequency: med.frequency,
      field_medication_starting_date: med.startingDate,
      field_medication_status: med.status,
      id: med.id,
    });
  }

  if (goal.date && goal.name && goal.status) {
    return mapDataToEntityFormat({
      field_goal_name: goal.name,
      field_goal_target_date: goal.date,
      field_goal_status: goal.status,
      id: goal.id,
    });
  }

  if (note.title && note.dueDate && note.body) {
    return mapDataToEntityFormat({
      field_title: note.title,
      field_notes_due_date: note.dueDate,
      field_body: note.body,
      id: note.id,
    });
  }

  if (sym.name) {
    return mapDataToEntityFormat({
      field_symptom_name: sym.name,
      id: sym.id,
    });
  }

  return {};
};

export const fromEntityData = (data: any): Object => {
  if ("field_medication_label" in data) {
    return {
      name: data.field_medication_label[0].value,
      frequency: data.field_medication_frequency[0].value,
      startingDate: data.field_medication_starting_date[0].value,
      status: data.field_medication_status[0].value,
      id: data.id[0].value,
    };
  } else if ("field_goal_name" in data) {
    return {
      name: data.field_goal_name[0].value,
      date: data.field_goal_target_date[0].value,
      status: data.field_goal_status[0].value,
      id: data.id[0].value,
    };
  } else if ("field_notes_due_date" in data) {
    return {
      title: data.field_title[0].value,
      dueDate: data.field_notes_due_date[0].value,
      body: data.field_body[0].value,
      id: data.id[0].value,
    };
  } else {
    return {
      name: data.field_symptom_name[0].value,
      id: data.id[0].value,
    };
  }
};

export default mapDataToEntityFormat;
