import { createMuiTheme, CssBaseline, ThemeProvider } from "@material-ui/core";
import i18n from "i18next";
import { default as React } from "react";
import { initReactI18next } from "react-i18next";
import { Provider } from "react-redux";
import "./App.css";
import { Routes } from "./routing/Routes";
import { store } from "./store/Reducer";

const theme = createMuiTheme({
  overrides: {
    MuiCssBaseline: {
      "@global": {
        html: {
          WebkitFontSmoothing: "auto",
        },
      },
    },
    MuiDialog: {
      paper: {
        minWidth: "580px",
      },
    },
  },
});

const App = () => {
  i18n
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
      resources: {
        en: {
          translation: {
            "Welcome to React": "Welcome to React and react-i18next",
          },
        },
      },
      lng: "en",
      fallbackLng: "en",

      interpolation: {
        escapeValue: false,
      },
    });

  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Routes />
      </ThemeProvider>
    </Provider>
  );
};

export default App;
